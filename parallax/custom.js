var header = document.querySelector('.header');
var burger = document.querySelector('.burger');
var mobileMenu = document.querySelector('.mobile-menu');


// Scroll efektas headerio
window.addEventListener('scroll', function() {

	if(window.scrollY >= 500) {
		header.classList.add('header-show');
	} else {
		header.classList.remove('header-show');
	}

});


// Mobile menu burgerio
burger.addEventListener('click', function() {
	burger.classList.toggle('burger-expand');
	mobileMenu.classList.toggle('mobile-menu-expand');
});

$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
  	items: 1,
  	loop: true,
  	autoplay: true,
  	autoplayTimeout:5000,
	autoplayHoverPause:false,
  	animateOut: 'fadeOut',
  	mouseDrag: false,
  	touchDrag: false,
  	pullDrag: false,
  	freeDrag: false
  });
});


// Movement - parallax
// formulė primityvi: scroll pozicija * koeficiento / 100
// 
// Logika: kai scrollinam ir musu pirmas elementas yra netoli 
// scroll pozicijos
// pradedam stumdyt elementus
// gauti div Y koordinates visame puslapyje - getBoundingClientRect().top
// 


var items = document.querySelectorAll('[data-kof]');
// console.log(items);


window.addEventListener('scroll', function() {

	// perbegti per visus items elementus
	// kai perbegu per kiekviena
	// as noriu patikrinti ar jis yra netoli scroll pos
	
	for(var i = 0; i < items.length; i++) {

		var itemTop = items[i].getBoundingClientRect().top;


		if(window.scrollY - 100 > itemTop) {
			
			var kof = items[i].dataset.kof; 
			var result = kof * window.scrollY / 200;
			// console.log(result);

			// css stilius transform: translateY(10px)
			// translate(x px, y px)
			items[i].style.transform = 'translateY(' + result + 'px)';

		}

	}

});



// Popup

// var data = document.getElementsByClassName('item');


// function popup(id) {

// }



window.addEventListener('resize', function() {
	console.log('resize');
	if(window.innerWidth < 990) {
		console.log('mazas langas');
		document.querySelector('body').textContent = 'Nusipirk plansete';
	}
});